// SPDX-License-Identifier: MIT

package photometadata

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"unsafe"

	"gitlab.com/lookoutpoint/photometadata/internal/tiff"

	log "github.com/sirupsen/logrus"
)

// ExifData is a map of extracted Exif data
// This must be binary-compatible with tiff.Metadata
type ExifData map[ExifTag]interface{}

// Metadata holds all photo metadata (dimensions, Exif and XMP)
type Metadata struct {
	Width  int
	Height int
	Exif   ExifData
	Xmp    XmpData
}

// NewMetadata creates a new empty Metadata object
func NewMetadata() *Metadata {
	return &Metadata{
		Exif: make(ExifData),
	}
}

// Rational number (used in Exif values)
type Rational = tiff.IfdRational

// Urational is an unsigned rational number (used in Exif values)
type Urational = tiff.IfdUrational

// Extract photo metadata from the given io.Reader
func Extract(rd io.Reader) (*Metadata, error) {
	return ExtractWithLog(rd, log.NewEntry(log.StandardLogger()))
}

// ExtractWithLog extracts photo metadata from the given io.Reader and uses the provided logger for log messages
func ExtractWithLog(rd io.Reader, l *log.Entry) (*Metadata, error) {
	l = l.WithField("photometadata-func", "ExtractWithLog")

	reader := bufio.NewReader(rd)

	metadata := NewMetadata()

	l.Trace("Running Extract")

	// Read segments
	for {
		segmentType, dataLen, err := readSegmentHeader(reader)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		l.Tracef("Read segment type=0x%02x, len=%d", segmentType, dataLen)

		if segmentType >= 0xc0 && segmentType <= 0xcf {
			if segmentType == 0xc0 {
				// SOF: read number of vertical and horizontal lines (height and width)
				// Format: XX H0 H1 W0 W1
				data := make([]byte, 5)
				_, err := io.ReadFull(reader, data)
				if err != nil {
					return nil, err
				}

				metadata.Width = int((uint(data[3]) << 8) | uint(data[4]))
				metadata.Height = int((uint(data[1]) << 8) | uint(data[2]))
			}

			// Start of frame segment. Don't look for metadata beyond this point.
			l.Trace("Hit start of frame segment - stopping")
			break
		}

		if segmentType == 0xe1 {
			// App1 data segment. Look for EXIF and XMP data here.
			l.Trace("Hit APP1 segment")
			err = ProcessApp1Segment(reader, dataLen, metadata, l)
			if err != nil {
				l.WithField("err", err).Warn("Failed to process APP1 segment, continuing")
			}
		} else {
			// Some other segment, skip over it.
			l.Trace("Skipping segment")
			err = skip(reader, dataLen)
			if err != nil {
				return nil, err
			}
		}
	}

	return metadata, nil
}

// ExtractApp1Segments returns all APP1 segments found in the provided reader data
func ExtractApp1Segments(rd io.Reader) ([][]byte, error) {
	return ExtractApp1SegmentsWithLog(rd, log.NewEntry(log.StandardLogger()))
}

// ExtractApp1SegmentsWithLog returns all APP1 segments found in the provided reader data and uses the provided logger
func ExtractApp1SegmentsWithLog(rd io.Reader, l *log.Entry) ([][]byte, error) {
	l = l.WithField("photometadata-func", "ExtractApp1SegmentsWithLog")

	reader := bufio.NewReader(rd)

	l.Trace("Running Extract")

	segments := [][]byte{}

	// Read segments
	for {
		segmentType, dataLen, err := readSegmentHeader(reader)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		l.Tracef("Read segment type=0x%02x, len=%d", segmentType, dataLen)

		if segmentType >= 0xc0 && segmentType <= 0xcf {
			// Start of frame segment. Nothing more to search.
			l.Trace("Hit start of frame segment - stopping")
			break
		}

		if segmentType == 0xe1 {
			// App1 data segment.
			l.Trace("Hit APP1 segment")

			// Rebuild segment header
			// Data length in header includes the data length field itself, while dataLen
			// does not include the field
			segment := make([]byte, dataLen+4)
			segment[0] = 0xff
			segment[1] = 0xe1
			segment[2] = byte(((dataLen + 2) >> 8) & 0xff)
			segment[3] = byte((dataLen + 2) & 0xff)

			nr, err := io.ReadFull(reader, segment[4:])
			if err != nil {
				l.WithField("err", err).Error("Could not read APP1 segment data")
				return nil, err
			} else if nr != dataLen {
				l.Errorf("Did not read all bytes for segment (%v vs. %v)", nr, dataLen)
				return nil, errors.New("Did not read all bytes for segment")
			}

			segments = append(segments, segment)
		} else {
			// Some other segment, skip over it.
			l.Trace("Skipping segment")
			err = skip(reader, dataLen)
			if err != nil {
				return nil, err
			}
		}
	}

	return segments, nil
}

func readSegmentHeader(reader *bufio.Reader) (byte, int, error) {
	// Read segment marker
	// Two bytes: 0xff, <segment marker>
	marker := make([]byte, 2)
	_, err := io.ReadFull(reader, marker)
	if err != nil {
		return 0, 0, err
	} else if marker[0] != 0xff {
		return 0, 0, fmt.Errorf("readSegmentHeader: segment header should start with 0xff (found 0x%02x)", marker[0])
	}

	segMarker := marker[1]

	// Some segments don't have any payload (see https://www.w3.org/Graphics/JPEG/itu-t81.pdf)
	// 0xd0 = 0xd7: restart
	// 0xd8: start of image
	// 0xd9: end of image
	// 0x01: reserved
	if (segMarker >= 0xd0 && segMarker <= 0xd9) || segMarker == 0x01 {
		return segMarker, 0, nil
	}

	// This segment has a payload, get the length which is the next two bytes
	segLength := make([]byte, 2)
	_, err = io.ReadFull(reader, segLength)
	if err != nil {
		return 0, 0, err
	}

	dataLen := int(segLength[0])*256 + int(segLength[1]) - 2

	return segMarker, dataLen, nil
}

func skip(reader *bufio.Reader, n int) error {
	_, err := reader.Discard(n)
	if err != nil {
		return err
	}
	return nil
}

var app1ExifMarker = []byte("Exif\x00\x00")
var app1XMPMarker = []byte("http://ns.adobe.com/xap/1.0/\x00")

// ProcessApp1Segment parses EXIF or XMP data from the reader assuming it contains the data for an APP1 segment
// The resulting metadata is stored in the provided metadata object
func ProcessApp1Segment(reader io.Reader, dataLen int, metadata *Metadata, l *log.Entry) error {
	// Reference: https://www.exif.org/Exif2-2.PDF

	data := make([]byte, dataLen)
	_, err := io.ReadFull(reader, data)
	if err != nil {
		return err
	}

	// Check for Exif data
	if len(data) >= len(app1ExifMarker) && bytes.Compare(data[:len(app1ExifMarker)], app1ExifMarker) == 0 {
		log.Trace("Parsing Exif APP1 segment")
		tiffData := tiff.New(data[len(app1ExifMarker):])
		return tiffData.Parse(*(*tiff.Metadata)(unsafe.Pointer(&metadata.Exif)), l)
	}

	// Check for XMP data
	if len(data) >= len(app1XMPMarker) && bytes.Compare(data[:len(app1XMPMarker)], app1XMPMarker) == 0 {
		log.Trace("Parsing XMP APP1 segment")
		err := parseXMP(data[len(app1XMPMarker):], &metadata.Xmp)
		return err
	}

	return fmt.Errorf("Unrecognized APP1 segment")
}
