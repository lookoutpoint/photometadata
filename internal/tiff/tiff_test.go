// SPDX-License-Identifier: MIT

package tiff

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var err error

var _ = Describe("Tiff", func() {
	data := []byte{1, 2, 3, 4, 5, 6, 7, 8}

	leTiff := &Data{data: data, littleEndian: true}
	beTiff := &Data{data: data, littleEndian: false}

	It("readUint8", func() {
		for i := 0; i < len(data); i++ {
			Expect(leTiff.readUint8(i)).To(BeEquivalentTo(i + 1))
			Expect(beTiff.readUint8(i)).To(BeEquivalentTo(i + 1))
		}

		_, err = leTiff.readUint8(8)
		Expect(err).To(HaveOccurred())

		_, err = leTiff.readUint8(-1)
		Expect(err).To(HaveOccurred())

		_, err = beTiff.readUint8(8)
		Expect(err).To(HaveOccurred())

		_, err = beTiff.readUint8(-1)
		Expect(err).To(HaveOccurred())
	})

	It("readUint16", func() {
		Expect(leTiff.readUint16(0)).To(BeEquivalentTo(0x0201))
		Expect(leTiff.readUint16(1)).To(BeEquivalentTo(0x0302))
		Expect(leTiff.readUint16(2)).To(BeEquivalentTo(0x0403))
		Expect(leTiff.readUint16(3)).To(BeEquivalentTo(0x0504))
		Expect(leTiff.readUint16(4)).To(BeEquivalentTo(0x0605))
		Expect(leTiff.readUint16(5)).To(BeEquivalentTo(0x0706))
		Expect(leTiff.readUint16(6)).To(BeEquivalentTo(0x0807))

		_, err = leTiff.readUint16(7)
		Expect(err).To(HaveOccurred())

		_, err = leTiff.readUint16(-1)
		Expect(err).To(HaveOccurred())

		Expect(beTiff.readUint16(0)).To(BeEquivalentTo(0x0102))
		Expect(beTiff.readUint16(1)).To(BeEquivalentTo(0x0203))
		Expect(beTiff.readUint16(2)).To(BeEquivalentTo(0x0304))
		Expect(beTiff.readUint16(3)).To(BeEquivalentTo(0x0405))
		Expect(beTiff.readUint16(4)).To(BeEquivalentTo(0x0506))
		Expect(beTiff.readUint16(5)).To(BeEquivalentTo(0x0607))
		Expect(beTiff.readUint16(6)).To(BeEquivalentTo(0x0708))

		_, err = beTiff.readUint16(7)
		Expect(err).To(HaveOccurred())

		_, err = beTiff.readUint16(-1)
		Expect(err).To(HaveOccurred())
	})

	It("readUint32", func() {
		Expect(leTiff.readUint32(0)).To(BeEquivalentTo(0x04030201))
		Expect(leTiff.readUint32(1)).To(BeEquivalentTo(0x05040302))
		Expect(leTiff.readUint32(2)).To(BeEquivalentTo(0x06050403))
		Expect(leTiff.readUint32(3)).To(BeEquivalentTo(0x07060504))
		Expect(leTiff.readUint32(4)).To(BeEquivalentTo(0x08070605))

		_, err = leTiff.readUint32(5)
		Expect(err).To(HaveOccurred())

		_, err = leTiff.readUint32(-1)
		Expect(err).To(HaveOccurred())

		Expect(beTiff.readUint32(0)).To(BeEquivalentTo(0x01020304))
		Expect(beTiff.readUint32(1)).To(BeEquivalentTo(0x02030405))
		Expect(beTiff.readUint32(2)).To(BeEquivalentTo(0x03040506))
		Expect(beTiff.readUint32(3)).To(BeEquivalentTo(0x04050607))
		Expect(beTiff.readUint32(4)).To(BeEquivalentTo(0x05060708))

		_, err = beTiff.readUint32(5)
		Expect(err).To(HaveOccurred())

		_, err = beTiff.readUint32(-1)
		Expect(err).To(HaveOccurred())
	})
})
