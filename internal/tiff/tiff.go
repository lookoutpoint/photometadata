// SPDX-License-Identifier: MIT

package tiff

import (
	"encoding/binary"
	"fmt"

	log "github.com/sirupsen/logrus"
)

// Tag is the identifier for a Tiff/Exif tag
//go:generate stringer -type=Tag -trimprefix Tag
type Tag uint16

// Internal tags
const (
	TagExifOffset             Tag = 0x8769
	TagGpsOffset              Tag = 0x8825
	TagInteroperabilityOffset Tag = 0xA005
)

// Metadata is a map type to store extracted metadata
type Metadata map[Tag]interface{}

// Data represents raw TIFF data
type Data struct {
	data []byte

	littleEndian bool
}

// New creates a TIFF raw data wrapper
func New(data []byte) *Data {
	return &Data{
		data: data,
	}
}

// Parse extracts TIFF metadata
func (td *Data) Parse(md Metadata, l *log.Entry) error {
	// Parse header
	ifd0Offset, err := td.parseHeader(l)
	if err != nil {
		return err
	}

	// Read IFD0. Don't care about the next IFD that is linked.
	_, err = td.parseIfd(ifd0Offset, md)

	// Check for the EXIF SubIFD
	exifIfdOffset, ok := md[TagExifOffset]
	if ok {
		var offset int
		offset = int(exifIfdOffset.(uint32))
		for offset != 0 {
			offset, err = td.parseIfd(offset, md)
			if err != nil {
				return err
			}
		}
	}

	// Check for the GPS SubIFD
	gpsIfdOffset, ok := md[TagGpsOffset]
	if ok {
		var offset int
		offset = int(gpsIfdOffset.(uint32))
		for offset != 0 {
			offset, err = td.parseIfd(offset, md)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (td *Data) parseIfd(offset int, md Metadata) (int, error) {
	ifd, err := newIFD(td, offset)
	if err != nil {
		return 0, err
	}

	return ifd.Parse(md)
}

// Parses the TIFF header assuming it is located at the beginning of td.data.
// Returns offset to the first IFD upon success.
func (td *Data) parseHeader(l *log.Entry) (int, error) {
	// Header format:
	//	2 bytes: byte order
	//	2 bytes: 0x002a
	//	4 bytes: offset to first IFD

	// Byte order
	err := td.parseByteOrder(l)
	if err != nil {
		return 0, err
	}

	// Check fixed value
	fixedValue, err := td.readUint16(2)
	if fixedValue != 0x002a {
		return 0, fmt.Errorf("parseHeader: expected 0x2a00 but got 0x%04x", fixedValue)
	}

	// Read IFD offset and return
	ifdOffset, err := td.readUint32(4)
	return int(ifdOffset), err
}

func (td *Data) parseByteOrder(l *log.Entry) error {
	data := td.data

	// Next two bytes indicate byte ordering
	// 0x4949 = little endian
	// 0x4d4d = big endian
	byteOrderTag := data[0]
	if len(data) < 2 || data[1] != byteOrderTag {
		return fmt.Errorf("parseByteOrder: byte order tags mismatched")
	} else if byteOrderTag != 0x49 && byteOrderTag != 0x4d {
		return fmt.Errorf("parseByteOrder: unrecognized byte order tag 0x%02x", byteOrderTag)
	}

	td.littleEndian = byteOrderTag == 0x49
	l.Tracef("Little endian = %v, byte order tag: 0x%02x", td.littleEndian, byteOrderTag)

	return nil
}

func (td *Data) readRaw(offset int, n int) ([]byte, error) {
	return td.readRawBuf(td.data, offset, n)
}

func (td *Data) readRawBuf(buf []byte, offset int, n int) ([]byte, error) {
	if offset < 0 {
		return nil, fmt.Errorf("readRaw: negative offset (%d)", offset)
	} else if len(buf) < offset+n {
		return nil, fmt.Errorf("readRaw: not enough bytes (data length = %d, offset = %d, n = %d)", len(buf), offset, n)
	}

	return buf[offset : offset+n], nil
}

func (td *Data) readUint8(offset int) (uint8, error) {
	buffer, err := td.readRaw(offset, 1)
	if err != nil {
		return 0, err
	}

	return uint8(buffer[0]), nil
}

func (td *Data) readUint16(offset int) (uint16, error) {
	return td.readUint16Buf(td.data, offset)
}

func (td *Data) readUint16Buf(buf []byte, offset int) (uint16, error) {
	buffer, err := td.readRawBuf(buf, offset, 2)
	if err != nil {
		return 0, err
	}

	if td.littleEndian {
		return binary.LittleEndian.Uint16(buffer), nil
	} else {
		return binary.BigEndian.Uint16(buffer), nil
	}
}

func (td *Data) readUint32(offset int) (uint32, error) {
	return td.readUint32Buf(td.data, offset)
}

func (td *Data) readUint32Buf(buf []byte, offset int) (uint32, error) {
	buffer, err := td.readRawBuf(buf, offset, 4)
	if err != nil {
		return 0, err
	}

	if td.littleEndian {
		return binary.LittleEndian.Uint32(buffer), nil
	} else {
		return binary.BigEndian.Uint32(buffer), nil
	}
}
