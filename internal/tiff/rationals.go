// SPDX-License-Identifier: MIT

package tiff

import "fmt"

// Represents an unsigned rational number.
type IfdUrational struct {
	// Numerator
	Num uint32

	// Denominator
	Den uint32
}

// Represents a signed rational number.
type IfdRational struct {
	// Numerator
	Num int32

	// Denominator
	Den int32
}

func (r IfdUrational) String() string {
	return fmt.Sprintf("%v / %v", r.Num, r.Den)
}

func (r IfdRational) String() string {
	return fmt.Sprintf("%v / %v", r.Num, r.Den)
}
