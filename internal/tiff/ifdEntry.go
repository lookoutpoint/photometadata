// SPDX-License-Identifier: MIT

package tiff

import (
	"fmt"
	"unsafe"

	log "github.com/sirupsen/logrus"
)

type ifdEntry struct {
	ifd           *ifd
	tag           Tag
	format        ifdDataFormat
	numComponents int
	offsetData    []byte
}

// Interpret offsetData as a 32-bit offset
func (entry *ifdEntry) offset() int {
	value, err := entry.ifd.td.readUint32Buf(entry.offsetData, 0)
	if err != nil {
		log.WithField("err", err).Panicf("ifdEntry.offset: could not read offset")
	}
	return int(value)
}

func (entry *ifdEntry) readValue() (interface{}, error) {
	switch entry.format {
	case dfmtUint8:
		return entry.readUint8()

	case dfmtInt8:
		return entry.readInt8()

	case dfmtString:
		return entry.readString()

	case dfmtUint16:
		return entry.readUint16()

	case dfmtInt16:
		return entry.readInt16()

	case dfmtUint32:
		return entry.readUint32()

	case dfmtInt32:
		return entry.readInt32()

	case dfmtUrational:
		return entry.readUrational()

	case dfmtRational:
		return entry.readRational()

	case dfmtUndefined:
		return entry.readUndefined()
	}

	return nil, fmt.Errorf("readValue: unknown data format (%v)", entry.format)
}

func (entry *ifdEntry) readUint8() (interface{}, error) {
	if entry.numComponents == 1 {
		// Return a single uint8 using offset as data
		return entry.offsetData[0], nil
	} else if entry.numComponents <= 4 {
		// Use offset as data
		return entry.offsetData[:entry.numComponents], nil
	} else {
		// Data is stored at an offset relative to the TIFF header.
		offset := entry.offset()
		return entry.ifd.td.readRaw(offset, entry.numComponents)
	}
}

func (entry *ifdEntry) readInt8() (interface{}, error) {
	value, err := entry.readUint8()
	if err != nil {
		return nil, err
	}

	if entry.numComponents == 1 {
		return int8(value.(uint8)), nil
	} else {
		a := value.([]uint8)
		return *(*[]int8)(unsafe.Pointer(&a)), nil
	}
}

func (entry *ifdEntry) readString() (interface{}, error) {
	var bytes []byte
	if entry.numComponents <= 4 {
		// Use offset as data
		bytes = entry.offsetData
	} else {
		var err error
		bytes, err = entry.ifd.td.readRaw(entry.offset(), entry.numComponents)
		if err != nil {
			return nil, err
		}
	}

	// Last byte should be a null byte
	if bytes[entry.numComponents-1] != 0 {
		return nil, fmt.Errorf("readValue[string]: not null terminated (%v %v)", bytes[entry.numComponents-1], string(bytes))
	}

	return string(bytes[:entry.numComponents-1]), nil
}

func (entry *ifdEntry) readUint16() (interface{}, error) {
	if entry.numComponents == 1 {
		// Use offset as data
		return entry.ifd.td.readUint16Buf(entry.offsetData, 0)
	} else {
		values := make([]uint16, entry.numComponents)
		if entry.numComponents == 2 {
			// Use offset as data
			var err error
			for i := 0; i < 2; i++ {
				values[i], err = entry.ifd.td.readUint16Buf(entry.offsetData, i*2)
				if err != nil {
					return nil, err
				}
			}
		} else {
			// Read from offset
			offset := entry.offset()
			for i := 0; i < entry.numComponents; i++ {
				value, err := entry.ifd.td.readUint16(offset + i*2)
				if err != nil {
					return nil, err
				}
				values[i] = value
			}
		}

		return values, nil
	}
}

func (entry *ifdEntry) readInt16() (interface{}, error) {
	value, err := entry.readUint16()
	if err != nil {
		return nil, err
	}

	if entry.numComponents == 1 {
		return int16(value.(uint16)), nil
	} else {
		a := value.([]uint16)
		return *(*[]int16)(unsafe.Pointer(&a)), nil
	}
}

func (entry *ifdEntry) readUint32() (interface{}, error) {
	if entry.numComponents == 1 {
		// Use offset as data
		return entry.ifd.td.readUint32Buf(entry.offsetData, 0)
	} else {
		values := make([]uint32, entry.numComponents)

		// Read from offset
		offset := entry.offset()
		for i := 0; i < entry.numComponents; i++ {
			value, err := entry.ifd.td.readUint32(offset + i*4)
			if err != nil {
				return nil, err
			}
			values[i] = value
		}

		return values, nil
	}
}

func (entry *ifdEntry) readInt32() (interface{}, error) {
	value, err := entry.readUint32()
	if err != nil {
		return nil, err
	}

	if entry.numComponents == 1 {
		return int32(value.(uint32)), nil
	} else {
		a := value.([]uint32)
		return *(*[]int32)(unsafe.Pointer(&a)), nil
	}
}

func (entry *ifdEntry) readUrational() (interface{}, error) {
	offset := entry.offset()

	if entry.numComponents == 1 {
		value := IfdUrational{}
		err := entry.readUrational1(offset, &value)
		if err != nil {
			return nil, err
		}

		return value, nil
	} else {
		values := make([]IfdUrational, entry.numComponents)
		for i := 0; i < entry.numComponents; i++ {
			err := entry.readUrational1(offset+i*8, &values[i])
			if err != nil {
				return nil, err
			}
		}

		return values, nil
	}
}

func (entry *ifdEntry) readRational() (interface{}, error) {
	value, err := entry.readUrational()
	if err != nil {
		return nil, err
	}

	if entry.numComponents == 1 {
		v := value.(IfdUrational)
		return *(*IfdRational)(unsafe.Pointer(&v)), nil
	} else {
		a := value.([]IfdUrational)
		return *(*[]IfdRational)(unsafe.Pointer(&a)), nil
	}
}

func (entry *ifdEntry) readUrational1(offset int, value *IfdUrational) error {
	var err error

	value.Num, err = entry.ifd.td.readUint32(offset)
	if err != nil {
		return err
	}

	value.Den, err = entry.ifd.td.readUint32(offset + 4)
	return err
}

func (entry *ifdEntry) readUndefined() (interface{}, error) {
	if entry.numComponents <= 4 {
		// Use offset as data
		return entry.offsetData[:entry.numComponents], nil
	} else {
		return entry.ifd.td.readRaw(entry.offset(), entry.numComponents)
	}
}
