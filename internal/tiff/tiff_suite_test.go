// SPDX-License-Identifier: MIT

package tiff

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestTiff(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Tiff Suite")
}
