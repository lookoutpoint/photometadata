// SPDX-License-Identifier: MIT

package tiff

import (
	"fmt"
)

type ifdDataFormat uint16

const (
	dfmtUint8 ifdDataFormat = iota + 1
	dfmtString
	dfmtUint16
	dfmtUint32
	dfmtUrational
	dfmtInt8
	dfmtUndefined
	dfmtInt16
	dfmtInt32
	dfmtRational
	dfmtFloat
	dfmtDouble
)

type ifd struct {
	td     *Data
	offset int
}

func newIFD(td *Data, offset int) (*ifd, error) {
	if len(td.data) < offset+1 {
		return nil, fmt.Errorf("newIfd: offset (%d) exceeds data length (%d)", offset, len(td.data))
	}

	return &ifd{
		td:     td,
		offset: offset,
	}, nil
}

func (ifd *ifd) Parse(md Metadata) (int, error) {
	numEntries, err := ifd.getNumEntries()
	if err != nil {
		return 0, err
	}

	for i := 0; i < numEntries; i++ {
		entry, err := ifd.getEntry(i)
		if err != nil {
			return 0, err
		}

		entryValue, err := entry.readValue()
		if err != nil {
			return 0, err
		}

		md[entry.tag] = entryValue
	}

	// Read offset to next IFD
	nextIfdOffset, err := ifd.readUint32(2 + numEntries*12)
	return int(nextIfdOffset), err
}

func (ifd *ifd) readRaw(offset int, n int) ([]byte, error) {
	return ifd.td.readRaw(ifd.offset+offset, n)
}

func (ifd *ifd) readUint8(offset int) (uint8, error) {
	return ifd.td.readUint8(ifd.offset + offset)
}

func (ifd *ifd) readUint16(offset int) (uint16, error) {
	return ifd.td.readUint16(ifd.offset + offset)
}

func (ifd *ifd) readUint32(offset int) (uint32, error) {
	return ifd.td.readUint32(ifd.offset + offset)
}

func (ifd *ifd) getNumEntries() (int, error) {
	// Number of entries is first two bytes of the IFD
	numEntries, err := ifd.readUint16(0)
	return int(numEntries), err
}

func (ifd *ifd) getEntry(i int) (*ifdEntry, error) {
	entryOffset := 2 + 12*i

	tag, err := ifd.readUint16(entryOffset)
	if err != nil {
		return nil, err
	}

	format, err := ifd.readUint16(entryOffset + 2)
	if err != nil {
		return nil, err
	}

	numComponents, err := ifd.readUint32(entryOffset + 4)
	if err != nil {
		return nil, err
	}

	offsetData, err := ifd.readRaw(entryOffset+8, 4)
	if err != nil {
		return nil, err
	}

	return &ifdEntry{
		ifd:           ifd,
		tag:           Tag(tag),
		format:        ifdDataFormat(format),
		numComponents: int(numComponents),
		offsetData:    offsetData,
	}, nil
}
