// SPDX-License-Identifier: MIT

package tiff

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Ifd", func() {
	It("Uint8", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 4, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 1, 0, 1, 0, 0, 0, 0xf0, 0, 0, 0,
			/* 22 */ 101, 0, 1, 0, 4, 0, 0, 0, 1, 2, 3, 4,
			/* 34 */ 102, 0, 1, 0, 3, 0, 0, 0, 10, 20, 30, 0,
			/* 46 */ 103, 0, 1, 0, 6, 0, 0, 0, 58, 0, 0, 0,
			// data
			/* 58 */ 6, 5, 4, 3, 2, 1,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(4))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtUint8))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(uint8(0xf0)))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtUint8))
		Expect(entry1.numComponents).To(Equal(4))
		Expect(entry1.readValue()).To(Equal([]uint8{1, 2, 3, 4}))

		entry2, _ := ifd.getEntry(2)
		Expect(entry2.tag).To(BeEquivalentTo(102))
		Expect(entry2.format).To(Equal(dfmtUint8))
		Expect(entry2.numComponents).To(Equal(3))
		Expect(entry2.readValue()).To(Equal([]uint8{10, 20, 30}))

		entry3, _ := ifd.getEntry(3)
		Expect(entry3.tag).To(BeEquivalentTo(103))
		Expect(entry3.format).To(Equal(dfmtUint8))
		Expect(entry3.numComponents).To(Equal(6))
		Expect(entry3.readValue()).To(Equal([]uint8{6, 5, 4, 3, 2, 1}))
	})

	It("Int8", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 4, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 6, 0, 1, 0, 0, 0, 0xf0, 0, 0, 0,
			/* 22 */ 101, 0, 6, 0, 4, 0, 0, 0, 1, 2, 3, 4,
			/* 34 */ 102, 0, 6, 0, 3, 0, 0, 0, 10, 20, 30, 0,
			/* 46 */ 103, 0, 6, 0, 6, 0, 0, 0, 58, 0, 0, 0,
			// data
			/* 58 */ 6, 5, 4, 3, 2, 1,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(4))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtInt8))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(int8(-16)))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtInt8))
		Expect(entry1.numComponents).To(Equal(4))
		Expect(entry1.readValue()).To(Equal([]int8{1, 2, 3, 4}))

		entry2, _ := ifd.getEntry(2)
		Expect(entry2.tag).To(BeEquivalentTo(102))
		Expect(entry2.format).To(Equal(dfmtInt8))
		Expect(entry2.numComponents).To(Equal(3))
		Expect(entry2.readValue()).To(Equal([]int8{10, 20, 30}))

		entry3, _ := ifd.getEntry(3)
		Expect(entry3.tag).To(BeEquivalentTo(103))
		Expect(entry3.format).To(Equal(dfmtInt8))
		Expect(entry3.numComponents).To(Equal(6))
		Expect(entry3.readValue()).To(Equal([]int8{6, 5, 4, 3, 2, 1}))
	})

	It("String", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 3, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 2, 0, 6, 0, 0, 0, 46, 0, 0, 0,
			/* 22 */ 101, 0, 2, 0, 2, 0, 0, 0, 'a', 0, 0, 0,
			/* 34 */ 102, 0, 2, 0, 4, 0, 0, 0, 'x', 'y', 'z', 0,
			// data
			/* 46 */ 'h', 'e', 'l', 'l', 'o', 0,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(3))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtString))
		Expect(entry0.numComponents).To(Equal(6))
		Expect(entry0.readValue()).To(Equal("hello"))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtString))
		Expect(entry1.numComponents).To(Equal(2))
		Expect(entry1.readValue()).To(Equal("a"))

		entry2, _ := ifd.getEntry(2)
		Expect(entry2.tag).To(BeEquivalentTo(102))
		Expect(entry2.format).To(Equal(dfmtString))
		Expect(entry2.numComponents).To(Equal(4))
		Expect(entry2.readValue()).To(Equal("xyz"))
	})

	It("Uint16", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 3, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			// entries
			/* 10 */ 100, 0, 3, 0, 1, 0, 0, 0, 0xff, 0x11, 0, 0,
			/* 22 */ 101, 0, 3, 0, 2, 0, 0, 0, 0xee, 0xdd, 0xaa, 0xbb,
			/* 34 */ 102, 0, 3, 0, 4, 0, 0, 0, 46, 0, 0, 0,
			// data
			/* 46 */ 0xfe, 0x10, 0xa0, 0xb0, 0xc0, 0xd0, 0x10, 0x20,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(3))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtUint16))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(uint16(0x11ff)))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtUint16))
		Expect(entry1.numComponents).To(Equal(2))
		Expect(entry1.readValue()).To(Equal([]uint16{0xddee, 0xbbaa}))

		entry2, _ := ifd.getEntry(2)
		Expect(entry2.tag).To(BeEquivalentTo(102))
		Expect(entry2.format).To(Equal(dfmtUint16))
		Expect(entry2.numComponents).To(Equal(4))
		Expect(entry2.readValue()).To(Equal([]uint16{0x10fe, 0xb0a0, 0xd0c0, 0x2010}))
	})

	It("Int16", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 3, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			// entries
			/* 10 */ 100, 0, 8, 0, 1, 0, 0, 0, 0xff, 0x11, 0, 0,
			/* 22 */ 101, 0, 8, 0, 2, 0, 0, 0, 0xee, 0xdd, 0xaa, 0xbb,
			/* 34 */ 102, 0, 8, 0, 4, 0, 0, 0, 46, 0, 0, 0,
			// data
			/* 46 */ 0xfe, 0x10, 0xa0, 0xb0, 0xc0, 0xd0, 0x10, 0x20,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(3))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtInt16))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(int16(0x11ff)))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtInt16))
		Expect(entry1.numComponents).To(Equal(2))
		Expect(entry1.readValue()).To(Equal([]int16{-8722, -17494}))

		entry2, _ := ifd.getEntry(2)
		Expect(entry2.tag).To(BeEquivalentTo(102))
		Expect(entry2.format).To(Equal(dfmtInt16))
		Expect(entry2.numComponents).To(Equal(4))
		Expect(entry2.readValue()).To(Equal([]int16{0x10fe, -20320, -12096, 0x2010}))
	})

	It("Uint32", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 2, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			// entries
			/* 10 */ 100, 0, 4, 0, 1, 0, 0, 0, 0x44, 0x33, 0x22, 0x11,
			/* 22 */ 101, 0, 4, 0, 3, 0, 0, 0, 34, 0, 0, 0,
			// data
			/* 34 */ 0xfe, 0x10, 0xa0, 0xb0, 0xc0, 0xd0, 0x10, 0x20, 0x01, 0x02, 0x03, 0x04,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(2))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtUint32))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(uint32(0x11223344)))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtUint32))
		Expect(entry1.numComponents).To(Equal(3))
		Expect(entry1.readValue()).To(Equal([]uint32{0xb0a010fe, 0x2010d0c0, 0x04030201}))
	})

	It("Int32", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 2, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			// entries
			/* 10 */ 100, 0, 9, 0, 1, 0, 0, 0, 0x44, 0x33, 0x22, 0x11,
			/* 22 */ 101, 0, 9, 0, 3, 0, 0, 0, 34, 0, 0, 0,
			// data
			/* 34 */ 0xfe, 0x10, 0xa0, 0xb0, 0xc0, 0xd0, 0x10, 0x20, 0x01, 0x02, 0x03, 0x04,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(2))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtInt32))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(int32(0x11223344)))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtInt32))
		Expect(entry1.numComponents).To(Equal(3))
		Expect(entry1.readValue()).To(Equal([]int32{-1331687170, 0x2010d0c0, 0x04030201}))
	})

	It("Urational", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 2, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 5, 0, 1, 0, 0, 0, 34, 0, 0, 0,
			/* 22 */ 101, 0, 5, 0, 2, 0, 0, 0, 42, 0, 0, 0,
			// data
			/* 34 */ 0xfe, 0x10, 0x00, 0x22, 0x44, 0x33, 0x22, 0x11,
			/* 42 */ 0xa0, 0xb0, 0xc0, 0xd0, 0x10, 0x20, 0x30, 0x40, 0x0f, 0x0f, 0x0f, 0x0f, 0x01, 0, 0, 0,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(2))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtUrational))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(IfdUrational{0x220010fe, 0x11223344}))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtUrational))
		Expect(entry1.numComponents).To(Equal(2))
		Expect(entry1.readValue()).To(Equal([]IfdUrational{{0xd0c0b0a0, 0x40302010}, {0x0f0f0f0f, 1}}))
	})

	It("Rational", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 2, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 10, 0, 1, 0, 0, 0, 34, 0, 0, 0,
			/* 22 */ 101, 0, 10, 0, 2, 0, 0, 0, 42, 0, 0, 0,
			// data
			/* 34 */ 0xfe, 0x10, 0x00, 0x22, 0x44, 0x33, 0x22, 0x11,
			/* 42 */ 0xa0, 0xb0, 0xc0, 0xd0, 0x10, 0x20, 0x30, 0x40, 0x0f, 0x0f, 0x0f, 0x0f, 0x01, 0, 0, 0,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(2))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtRational))
		Expect(entry0.numComponents).To(Equal(1))
		Expect(entry0.readValue()).To(Equal(IfdRational{0x220010fe, 0x11223344}))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtRational))
		Expect(entry1.numComponents).To(Equal(2))
		Expect(entry1.readValue()).To(Equal([]IfdRational{{-792678240, 0x40302010}, {0x0f0f0f0f, 1}}))
	})

	It("undefined", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 2, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 7, 0, 2, 0, 0, 0, 1, 2, 3, 4,
			/* 22 */ 101, 0, 7, 0, 5, 0, 0, 0, 34, 0, 0, 0,
			// data
			/* 34 */ 0x10, 0x20, 0x30, 0x40, 0x50,
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		Expect(ifd.getNumEntries()).To(Equal(2))

		entry0, _ := ifd.getEntry(0)
		Expect(entry0.tag).To(BeEquivalentTo(100))
		Expect(entry0.format).To(Equal(dfmtUndefined))
		Expect(entry0.numComponents).To(Equal(2))
		Expect(entry0.readValue()).To(Equal([]byte{1, 2}))

		entry1, _ := ifd.getEntry(1)
		Expect(entry1.tag).To(BeEquivalentTo(101))
		Expect(entry1.format).To(Equal(dfmtUndefined))
		Expect(entry1.numComponents).To(Equal(5))
		Expect(entry1.readValue()).To(Equal([]byte{0x10, 0x20, 0x30, 0x40, 0x50}))
	})

	It("next ifd offset", func() {
		data := []byte{
			/* 0 */ 0x49, 0x49, 0x2a, 0x00, 0x08, 0, 0, 0, // TIFF header
			/* 8 */ 4, 0, // num entries
			// entries: tag{x2}, format{x2}, numComponents{x4}, offsetOrData{x4}
			/* 10 */ 100, 0, 1, 0, 1, 0, 0, 0, 0xf0, 0, 0, 0,
			/* 22 */ 101, 0, 1, 0, 4, 0, 0, 0, 1, 2, 3, 4,
			/* 34 */ 102, 0, 1, 0, 3, 0, 0, 0, 10, 20, 30, 0,
			/* 46 */ 103, 0, 1, 0, 1, 0, 0, 0, 58, 0, 0, 0,
			/* 58 */ 0x11, 0x22, 0x33, 0x44, // offset
		}

		tiff := Data{data: data, littleEndian: true}
		ifd, _ := newIFD(&tiff, 8)

		md := make(Metadata)
		Expect(ifd.Parse(md)).To(Equal(0x44332211))
	})

})
