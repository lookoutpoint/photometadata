// SPDX-License-Identifier: MIT

package photometadata

//go:generate stringer -type=ExifTag -trimprefix ExifTag

// ExifTag is the type for all Exif tags
type ExifTag uint16

// Reference: https://www.exif.org/Exif2-2.PDF
const (
	// TIFF 6.0

	ExifTagImageWidth                ExifTag = 0x100
	ExifTagImageLength               ExifTag = 0x101
	ExifTagBitsPerSample             ExifTag = 0x102
	ExifTagCompression               ExifTag = 0x103
	ExifTagPhotometricInterpretation ExifTag = 0x106
	ExifTagOrientation               ExifTag = 0x112
	ExifTagSamplesPerPixel           ExifTag = 0x115
	ExifTagPlanarConfiguration       ExifTag = 0x11C
	ExifTagYCbCrSubSampling          ExifTag = 0x212
	ExifTagYCbCrPositioning          ExifTag = 0x213
	ExifTagXResolution               ExifTag = 0x11A
	ExifTagYResolution               ExifTag = 0x11B
	ExifTagResolutionUnit            ExifTag = 0x128

	ExifTagStripOffsets                ExifTag = 0x111
	ExifTagRowsPerStrip                ExifTag = 0x116
	ExifTagStripByteCounts             ExifTag = 0x117
	ExifTagJPEGInterchangeFormat       ExifTag = 0x201
	ExifTagJPEGInterchangeFormatLength ExifTag = 0x202

	ExifTagTransferFunction      ExifTag = 0x12D
	ExifTagWhitePoint            ExifTag = 0x13E
	ExifTagPrimaryChromaticities ExifTag = 0x13F
	ExifTagYCbCrCoefficients     ExifTag = 0x211
	ExifTagReferenceBlackWhite   ExifTag = 0x214

	ExifTagDateTime         ExifTag = 0x132
	ExifTagImageDescription ExifTag = 0x10E
	ExifTagMake             ExifTag = 0x10F
	ExifTagModel            ExifTag = 0x110
	ExifTagSoftware         ExifTag = 0x131
	ExifTagArtist           ExifTag = 0x13B
	ExifTagCopyright        ExifTag = 0x8298

	ExifTagExifOffset             ExifTag = 0x8769
	ExifTagGpsOffset              ExifTag = 0x8825
	ExifTagInteroperabilityOffset ExifTag = 0xA005

	// Exif IFD
	ExifTagExifVersion     ExifTag = 0x9000
	ExifTagFlashpixVersion ExifTag = 0xA000

	ExifTagColorSpace ExifTag = 0xA001

	ExifTagComponentsConfiguration ExifTag = 0x9101
	ExifTagCompressedBitsPerPixel  ExifTag = 0x9102
	ExifTagPixelXDimension         ExifTag = 0xA002
	ExifTagPixelYDimension         ExifTag = 0xA003

	ExifTagMakerNote   ExifTag = 0x927C
	ExifTagUserComment ExifTag = 0x9286

	ExifTagRelatedSoundFile ExifTag = 0xA004

	ExifTagDateTimeOriginal    ExifTag = 0x9003
	ExifTagDateTimeDigitized   ExifTag = 0x9004
	ExifTagSubSecTime          ExifTag = 0x9290
	ExifTagSubSecTimeOriginal  ExifTag = 0x9291
	ExifTagSubSecTimeDigitized ExifTag = 0x9292

	ExifTagExposureTime             ExifTag = 0x829A
	ExifTagFNumber                  ExifTag = 0x829D
	ExifTagExposureProgram          ExifTag = 0x8822
	ExifTagSpectralSensitivity      ExifTag = 0x8824
	ExifTagISOSpeedRatings          ExifTag = 0x8827
	ExifTagOECF                     ExifTag = 0x8828
	ExifTagShutterSpeedValue        ExifTag = 0x9201
	ExifTagApertureValue            ExifTag = 0x9202
	ExifTagBrightnessValue          ExifTag = 0x9203
	ExifTagExposureBiasValue        ExifTag = 0x9204
	ExifTagMaxApertureValue         ExifTag = 0x9205
	ExifTagSubjectDistance          ExifTag = 0x9206
	ExifTagMeteringMode             ExifTag = 0x9207
	ExifTagLightSource              ExifTag = 0x9208
	ExifTagFlash                    ExifTag = 0x9209
	ExifTagFocalLength              ExifTag = 0x920A
	ExifTagSubjectArea              ExifTag = 0x9214
	ExifTagFlashEnergy              ExifTag = 0xA20B
	ExifTagSpatialFrequencyResponse ExifTag = 0xA20C
	ExifTagFocalPlaneXResolution    ExifTag = 0xA20E
	ExifTagFocalPlaneYResolution    ExifTag = 0xA20F
	ExifTagFocalPlaneResolutionUnit ExifTag = 0xA210
	ExifTagSubjectLocation          ExifTag = 0xA214
	ExifTagExposureIndex            ExifTag = 0xA215
	ExifTagSensingMethod            ExifTag = 0xA217
	ExifTagFileSource               ExifTag = 0xA300
	ExifTagSceneType                ExifTag = 0xA301
	ExifTagCFAPattern               ExifTag = 0xA302
	ExifTagCustomRendered           ExifTag = 0xA401
	ExifTagExposureMode             ExifTag = 0xA402
	ExifTagWhiteBalance             ExifTag = 0xA403
	ExifTagDigitalZoomRatio         ExifTag = 0xA404
	ExifTagFocalLengthIn35mmFilm    ExifTag = 0xA405
	ExifTagSceneCaptureType         ExifTag = 0xA406
	ExifTagGainControl              ExifTag = 0xA407
	ExifTagContrast                 ExifTag = 0xA408
	ExifTagSaturation               ExifTag = 0xA409
	ExifTagSharpness                ExifTag = 0xA40A
	ExifTagDeviceSettingDescription ExifTag = 0xA40B
	ExifTagSubjectDistanceRange     ExifTag = 0xA40C

	ExifTagImageUniqueID ExifTag = 0xA420

	// Custom (not in any standard, but derived from examples)
	ExifTagLensModel ExifTag = 42036

	// this is unreliable
	// ExifTagTimeZoneOffset ExifTag = 36880

	// GPS IFD
	ExifTagGPSVersionID        ExifTag = 0x0
	ExifTagGPSLatitudeRef      ExifTag = 0x1
	ExifTagGPSLatitude         ExifTag = 0x2
	ExifTagGPSLongitudeRef     ExifTag = 0x3
	ExifTagGPSLongitude        ExifTag = 0x4
	ExifTagGPSAltitudeRef      ExifTag = 0x5
	ExifTagGPSAltitude         ExifTag = 0x6
	ExifTagGPSTimeStamp        ExifTag = 0x7
	ExifTagGPSSatellites       ExifTag = 0x8
	ExifTagGPSStatus           ExifTag = 0x9
	ExifTagGPSMeasureMode      ExifTag = 0xA
	ExifTagGPSDOP              ExifTag = 0xB
	ExifTagGPSSpeedRef         ExifTag = 0xC
	ExifTagGPSSpeed            ExifTag = 0xD
	ExifTagGPSTrackRef         ExifTag = 0xE
	ExifTagGPSTrack            ExifTag = 0xF
	ExifTagGPSImgDirectionRef  ExifTag = 0x10
	ExifTagGPSImgDirection     ExifTag = 0x11
	ExifTagGPSMapDatum         ExifTag = 0x12
	ExifTagGPSDestLatitudeRef  ExifTag = 0x13
	ExifTagGPSDestLatitude     ExifTag = 0x14
	ExifTagGPSDestLongitudeRef ExifTag = 0x15
	ExifTagGPSDestLongitude    ExifTag = 0x16
	ExifTagGPSDestBearingRef   ExifTag = 0x17
	ExifTagGPSDestBearing      ExifTag = 0x18
	ExifTagGPSDestDistanceRef  ExifTag = 0x19
	ExifTagGPSDestDistance     ExifTag = 0x1A
	ExifTagGPSProcessingMethod ExifTag = 0x1B
	ExifTagGPSAreaInformation  ExifTag = 0x1C
	ExifTagGPSDateStamp        ExifTag = 0x1D
	ExifTagGPSDifferential     ExifTag = 0x1E
)
