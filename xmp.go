// SPDX-License-Identifier: MIT

package photometadata

import (
	"encoding/xml"
)

type xmpMeta struct {
	XMLName        xml.Name `xml:"xmpmeta"`
	RDFDescription *XmpData `xml:"RDF>Description"`
}

// XmpData stores data extracted from XMP data. This corresponds to the rdf:Description element.
type XmpData struct {
	Title    string   `xml:"title>Alt>li"` // RDF format allows more than one
	Subjects []string `xml:"subject>Bag>li"`

	Location string `xml:"Location,attr"`
	Country  string `xml:"Country,attr"`
	State    string `xml:"State,attr"`
	City     string `xml:"City,attr"`

	UsageTerms         string `xml:"UsageTerms>Alt>li"` // RDF format allows more than one
	RightsWebStatement string `xml:"WebStatement,attr"`
	Rating             int    `xml:"Rating,attr"`
}

func parseXMP(data []byte, xmpData *XmpData) error {
	xmp := xmpMeta{RDFDescription: xmpData}
	err := xml.Unmarshal(data, &xmp)

	return err
}
