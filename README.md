# photo-metadata

EXIF and XMP metadata extraction from JPEG photo files

## License

MIT License. [Full license](LICENSE)
