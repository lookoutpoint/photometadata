// SPDX-License-Identifier: MIT

package photometadata_test

import (
	"bytes"
	"io"
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	log "github.com/sirupsen/logrus"

	. "gitlab.com/lookoutpoint/photometadata"
)

var _ = Describe("Metadata", func() {
	Describe("Extract", func() {
		r := func(n int32, d int32) Rational {
			return Rational{Num: n, Den: d}
		}

		ur := func(n uint32, d uint32) Urational {
			return Urational{Num: n, Den: d}
		}

		testExtract := func(fixtureName string) (*Metadata, *Metadata) {
			file, err := os.Open("fixtures/" + fixtureName)
			Expect(err).ToNot(HaveOccurred())
			defer file.Close()

			// Call Extract and ExtractApp1Segments
			// Need to split the stream
			rPipe, wPipe := io.Pipe()
			rFile := io.TeeReader(file, wPipe)

			extractChan := make(chan *Metadata)
			extractApp1Chan := make(chan *Metadata)

			// Goroutine for Extract
			go func() {
				defer GinkgoRecover()
				defer close(extractChan)

				md, err := Extract(rFile)
				Expect(err).ToNot(HaveOccurred())

				extractChan <- md
			}()

			// Goroutine for ExtractApp1Segments
			go func() {
				defer GinkgoRecover()
				defer close(extractApp1Chan)

				segments, err := ExtractApp1Segments(rPipe)
				Expect(err).ToNot(HaveOccurred())

				// Basic checks on segments and parse them
				md := NewMetadata()

				for _, segment := range segments {
					Expect(segment[0]).To(Equal(byte(0xff)))
					Expect(segment[1]).To(Equal(byte(0xe1)))

					dataLen := len(segment) - 2
					Expect(segment[2]).To(Equal(byte((dataLen >> 8) & 0xff)))
					Expect(segment[3]).To(Equal(byte(dataLen & 0xff)))

					err := ProcessApp1Segment(bytes.NewReader(segment[4:]), dataLen-2, md, log.NewEntry(log.StandardLogger()))
					Expect(err).ToNot(HaveOccurred())
				}

				extractApp1Chan <- md
			}()

			return <-extractChan, <-extractApp1Chan
		}

		It("nikon-via-train.jpg", func() {
			md, app1Md := testExtract("nikon-via-train.jpg")

			Expect(md.Width).To(BeNumerically("==", 3872))
			Expect(md.Height).To(BeNumerically("==", 2592))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal(`Via Train, Ontario
Early in the morning, on board the Vancouver-bound Via train on its way out of Toronto and north through Ontario.

Some quick exploration found me these "viewing cars", with tops that allow 360 degree viewing.  I would end up spending a lot of time in these spots.`))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("NIKON CORPORATION"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("NIKON D80"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2007 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(6643856, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(9, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(633985, 100000)))
				ExpectWithOffset(1, md.Exif[ExifTagMaxApertureValue]).To(Equal(ur(5357, 1482)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 100))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(0, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:23 15:45:30"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2007:06:23 09:36:16"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2007:06:23 09:36:16"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagLightSource]).To(BeNumerically("==", 9))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(18, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("AF-S DX Zoom-Nikkor 18-55mm f/3.5-5.6G ED"))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Beginning of the Journey"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(3))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal("Via Train, Ontario"))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"canada", "journal", "ontario", "train", "via"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("nikon-via-train2.jpg", func() {
			md, app1Md := testExtract("nikon-via-train2.jpg")

			Expect(md.Width).To(BeNumerically("==", 968))
			Expect(md.Height).To(BeNumerically("==", 648))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal(`Early in the morning, on board the Vancouver-bound Via train on its way out of Toronto and north through Ontario.

Some quick exploration found me these "viewing cars", with tops that allow 360 degree viewing.  I would end up spending a lot of time in these spots.`))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("NIKON CORPORATION"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("NIKON D80"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2007 Jason Wong."))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(6643856, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(9, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(633985, 100000)))
				ExpectWithOffset(1, md.Exif[ExifTagMaxApertureValue]).To(Equal(ur(5357, 1482)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 100))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(0, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:29 17:25:16"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2007:06:23 09:36:16"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2007:06:23 09:36:16"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagLightSource]).To(BeNumerically("==", 9))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(18, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("AF-S DX Zoom-Nikkor 18-55mm f/3.5-5.6G ED"))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Beginning of the Journey"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"canada", "journal", "ontario", "train", "via"}))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal("Via Train, Ontario"))
				ExpectWithOffset(1, md.Xmp.Country).To(Equal("Canada"))
				ExpectWithOffset(1, md.Xmp.State).To(Equal("Ontario"))
				ExpectWithOffset(1, md.Xmp.City).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(3))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("nikon1.jpg", func() {
			md, app1Md := testExtract("nikon1.jpg")

			Expect(md.Width).To(BeNumerically("==", 3173))
			Expect(md.Height).To(BeNumerically("==", 1741))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("Via Train, Alberta"))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("NIKON CORPORATION"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("NIKON D80"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2007 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 320)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(8321928, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(56, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(4970854, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagMaxApertureValue]).To(Equal(ur(36, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 200))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(0, 6)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:03:01 15:30:17"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2007:06:25 13:59:19"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2007:06:25 13:59:19"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagLightSource]).To(BeNumerically("==", 10))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(180, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("18.0-55.0 mm f/3.5-5.6"))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Tunneling Through the Rockies"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(4))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"__background", "alberta", "canada", "mountains", "train", "tunnel"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("nikon-gps.jpg", func() {
			md, app1Md := testExtract("nikon-gps.jpg")

			Expect(md.Width).To(BeNumerically("==", 3989))
			Expect(md.Height).To(BeNumerically("==", 1854))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("Látrabjarg, Iceland"))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("NIKON CORPORATION"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("NIKON D500"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2018 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 800)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(9643856, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(71, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(5655638, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagMaxApertureValue]).To(Equal(ur(50, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 100))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(0, 6)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:03:01 15:53:03"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2018:07:07 13:17:52"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2018:07:07 09:17:52"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagLightSource]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(850, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("16.0-85.0 mm f/3.5-5.6"))

				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitudeRef]).To(Equal("N"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitude]).To(Equal([]Urational{ur(65, 1), ur(294723000, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitudeRef]).To(Equal("W"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitude]).To(Equal([]Urational{ur(24, 1), ur(297759000, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSAltitudeRef]).To(Equal(uint8(0)))
				ExpectWithOffset(1, md.Exif[ExifTagGPSAltitude]).To(Equal(ur(216, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagGPSTimeStamp]).To(Equal([]Urational{ur(13, 1), ur(17, 1), ur(51400, 1000)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSSatellites]).To(Equal("10"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSImgDirectionRef]).To(Equal("M"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSImgDirection]).To(Equal(ur(22320, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagGPSDateStamp]).To(Equal("2018:07:07"))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Gliding"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(4))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"Látrabjarg", "__background", "animals", "bird", "flying", "fulmar", "gliding", "iceland", "westfjords", "wildlife", "wings"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("nikon-sioux-lookout.jpg", func() {
			md, app1Md := testExtract("nikon-sioux-lookout.jpg")

			Expect(md.Width).To(BeNumerically("==", 944))
			Expect(md.Height).To(BeNumerically("==", 632))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("One of the longer stops along the route, at Sioux Lookout. Last stop before exiting Ontario."))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("NIKON CORPORATION"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("NIKON D80"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2007 Jason Wong."))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 200)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(7643856, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(56, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(4970854, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagMaxApertureValue]).To(Equal(ur(5357, 1482)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 100))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(0, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:29 17:25:46"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2007:06:24 10:59:49"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2007:06:24 10:59:49"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagLightSource]).To(BeNumerically("==", 9))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(18, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 1))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("AF-S DX Zoom-Nikkor 18-55mm f/3.5-5.6G ED"))

				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitudeRef]).To(Equal("N"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitude]).To(Equal([]Urational{ur(50, 1), ur(58446667, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitudeRef]).To(Equal("W"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitude]).To(Equal([]Urational{ur(91, 1), ur(549660000, 10000000), ur(0, 1)}))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Sioux Lookout"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"__background", "canada", "journal", "ontario", "sioux lookout", "station", "train"}))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal("Sioux Lookout, Ontario"))
				ExpectWithOffset(1, md.Xmp.Country).To(Equal("Canada"))
				ExpectWithOffset(1, md.Xmp.State).To(Equal("Ontario"))
				ExpectWithOffset(1, md.Xmp.City).To(Equal("Sioux Lookout"))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(4))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("nikon-nz.jpg", func() {
			md, app1Md := testExtract("nikon-nz.jpg")

			Expect(md.Width).To(BeNumerically("==", 1392))
			Expect(md.Height).To(BeNumerically("==", 928))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("Island"))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("NIKON CORPORATION"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("NIKON D500"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2020 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 640)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(9321928, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(71, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(5655638, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagMaxApertureValue]).To(Equal(ur(42, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 50))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(-2, 6)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:23 23:01:53"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2020:01:12 09:39:19"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2020:01:12 09:39:19"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagLightSource]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(190, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("10.0-24.0 mm f/3.5-4.5"))

				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitudeRef]).To(Equal("S"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitude]).To(Equal([]Urational{ur(36, 1), ur(493608000, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitudeRef]).To(Equal("E"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitude]).To(Equal([]Urational{ur(174, 1), ur(485132000, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSAltitudeRef]).To(Equal(uint8(0)))
				ExpectWithOffset(1, md.Exif[ExifTagGPSAltitude]).To(Equal(ur(1, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagGPSTimeStamp]).To(Equal([]Urational{ur(20, 1), ur(39, 1), ur(19300, 1000)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSSatellites]).To(Equal("07"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSImgDirectionRef]).To(Equal("M"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSImgDirection]).To(Equal(ur(29550, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagGPSDateStamp]).To(Equal("2020:01:11"))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Island"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(3))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal("Auckland, New Zealand"))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"beach", "trip:New Zealand 2020"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("iphone1.jpg", func() {
			md, app1Md := testExtract("iphone1.jpg")

			Expect(md.Width).To(BeNumerically("==", 724))
			Expect(md.Height).To(BeNumerically("==", 965))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("Salt Lake City, Utah"))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("Apple"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("iPhone 8"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2019 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 15)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(3906891, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(18, 10)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(1695994, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagBrightnessValue]).To(Equal(r(43688, 22125)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureBiasValue]).To(Equal(r(0, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:23 18:51:00"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2019:04:03 20:56:59"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2019:04:03 20:56:59"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 5))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 16))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(399, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagLensModel]).To(Equal("iPhone 8 back camera 3.99mm f/1.8"))

				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitudeRef]).To(Equal("N"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitude]).To(Equal([]Urational{ur(40, 1), ur(463183333, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitudeRef]).To(Equal("W"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitude]).To(Equal([]Urational{ur(111, 1), ur(571133333, 10000000), ur(0, 1)}))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Dinner at Nomad Eatery"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(1))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"2019", "burger", "food", "fries", "nomad eatery", "salad", "tacos", "trips", "trips utah 2019", "utah"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("gopro1.jpg", func() {
			md, app1Md := testExtract("gopro1.jpg")

			Expect(md.Width).To(BeNumerically("==", 709))
			Expect(md.Height).To(BeNumerically("==", 410))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("Dixie National Forest, Utah"))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2019 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:23 19:02:17"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2019:04:04 14:55:03"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2015:01:02 07:35:50"))

				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitudeRef]).To(Equal("N"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitude]).To(Equal([]Urational{ur(37, 1), ur(444283525, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitudeRef]).To(Equal("W"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitude]).To(Equal([]Urational{ur(112, 1), ur(178921072, 10000000), ur(0, 1)}))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Tunnel"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(1))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"2019", "clouds", "driving", "highway", "rock", "trips", "trips utah 2019", "tunnel", "utah"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

		It("oneplus1.jpg", func() {
			md, app1Md := testExtract("oneplus1.jpg")

			Expect(md.Width).To(BeNumerically("==", 1018))
			Expect(md.Height).To(BeNumerically("==", 549))

			checkMd := func(md *Metadata) {
				ExpectWithOffset(1, md.Exif[ExifTagImageDescription]).To(Equal("Bryce Canyon NP, Utah"))
				ExpectWithOffset(1, md.Exif[ExifTagMake]).To(Equal("OnePlus"))
				ExpectWithOffset(1, md.Exif[ExifTagModel]).To(Equal("ONEPLUS A3000"))
				ExpectWithOffset(1, md.Exif[ExifTagXResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagYResolution]).To(Equal(ur(240, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagArtist]).To(Equal("Jason Wong"))
				ExpectWithOffset(1, md.Exif[ExifTagCopyright]).To(Equal("Copyright 2019 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Exif[ExifTagExposureTime]).To(Equal(ur(1, 25)))
				ExpectWithOffset(1, md.Exif[ExifTagShutterSpeedValue]).To(Equal(r(4643856, 1000000)))
				ExpectWithOffset(1, md.Exif[ExifTagFNumber]).To(Equal(ur(2, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagApertureValue]).To(Equal(ur(2, 1)))
				ExpectWithOffset(1, md.Exif[ExifTagISOSpeedRatings]).To(BeNumerically("==", 250))
				ExpectWithOffset(1, md.Exif[ExifTagBrightnessValue]).To(Equal(r(300, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagDateTime]).To(Equal("2020:05:23 22:50:48"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeOriginal]).To(Equal("2019:04:05 18:24:50"))
				ExpectWithOffset(1, md.Exif[ExifTagDateTimeDigitized]).To(Equal("2019:04:05 18:24:50"))
				ExpectWithOffset(1, md.Exif[ExifTagMeteringMode]).To(BeNumerically("==", 3))
				ExpectWithOffset(1, md.Exif[ExifTagFlash]).To(BeNumerically("==", 16))
				ExpectWithOffset(1, md.Exif[ExifTagFocalLength]).To(Equal(ur(426, 100)))
				ExpectWithOffset(1, md.Exif[ExifTagExposureMode]).To(BeNumerically("==", 0))
				ExpectWithOffset(1, md.Exif[ExifTagWhiteBalance]).To(BeNumerically("==", 0))

				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitudeRef]).To(Equal("N"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLatitude]).To(Equal([]Urational{ur(37, 1), ur(376241762, 10000000), ur(0, 1)}))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitudeRef]).To(Equal("W"))
				ExpectWithOffset(1, md.Exif[ExifTagGPSLongitude]).To(Equal([]Urational{ur(112, 1), ur(100786244, 10000000), ur(0, 1)}))

				ExpectWithOffset(1, md.Xmp.Title).To(Equal("Steak"))
				ExpectWithOffset(1, md.Xmp.Rating).To(Equal(1))
				ExpectWithOffset(1, md.Xmp.Location).To(Equal(""))
				ExpectWithOffset(1, md.Xmp.UsageTerms).To(Equal("Creative Commons BY-NC-SA"))
				ExpectWithOffset(1, md.Xmp.RightsWebStatement).To(Equal("https://creativecommons.org/licenses/by-nc-sa/4.0/"))
				ExpectWithOffset(1, md.Xmp.Subjects).To(Equal([]string{"2019", "bryce canyon", "food", "national park", "the lodge at bryce canyon", "trips", "trips utah 2019", "utah"}))
			}

			checkMd(md)
			checkMd(app1Md)
		})

	})
})
